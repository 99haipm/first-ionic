
import React, { useState } from 'react';
import { IonCard, IonCardContent, IonItem, IonIcon, IonLabel, IonButton,IonContent, IonToast } from '@ionic/react';
import { book } from 'ionicons/icons';
import '../../pages/list_books/ListBook.css';
import axios from 'axios';
const MyCard: React.FC = () => {
    const [name, setName] = useState('');
    const [desc, setDesc] = useState('');
    const [id, setId] = useState('');
    const [price,setPrice] = useState(0);
    const [data1, setData] = useState([]);
    const [status,setStatus] = useState(false);
    const [toast,SetToast] = useState(false);
    const fetchData = async () =>{

       await axios.get("http://192.168.169.177:63918/api/books").then( (data) => {setData(data.data)});
       setStatus(true);
    }

    const deleteBook = async () =>{
        
    }


    // var data = [
    //     {
    //         name: "Sách 1",
    //         desc: "Cuốn sách về FPTU...",
    //         id: "1",
    //     },
    //     {
    //         name: "Sách 2",
    //         desc: "Cuốn sách về FPT HN với các bạn nữ xinh đẹp ngành IT :))",
    //         id: "2",
    //     },
    //     {
    //         name: "Sách 3",
    //         desc: "Cuốn sách về FPT Cần Thơ mùa nước ahuhu...",
    //         id: "3",
    //     }
    // ]

    if(!status){
        fetchData();
    }
    var elm =  data1.map((item,index) => {
        return (
            <IonCard>
                <IonItem>
                    <IonIcon icon={book} slot="start" />
                    <IonLabel>{item["name"]}</IonLabel>
                    <IonButton color="success" slot="end" routerDirection="none" routerLink={`/page/edit:${item["id"]}`}>Detail</IonButton>
                    <IonButton color="danger" slot="end" onClick = {
                        async () => {
                            var response = await axios.delete(`http://192.168.169.177:63918/api/books?id=${item["id"]}`);
                            if(response.status == 200){
                                SetToast(true);
                                setStatus(false);
                            }
                        }
                    }>Delete</IonButton>
                </IonItem>
    
                <IonCardContent>
                    {item["description"]}
                </IonCardContent>
                <IonToast
                color = "success"
                isOpen={toast}
                onDidDismiss={() => SetToast(false)}
                message="Delete Success !"
                duration={500}
            />
            </IonCard>
        );
    })

    return (
        <IonContent>
            {elm}
        </IonContent>
    );
}

export default MyCard;