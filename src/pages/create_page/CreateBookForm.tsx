

import React, { useState } from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonInput, IonItem, IonLabel, IonList, IonItemDivider, IonTextarea, IonButton, IonCard, IonCardHeader, IonCardContent, IonCardTitle, IonText, IonButtons, IonMenuButton, IonToast } from '@ionic/react';
import axios from 'axios';
import { setInterval } from 'timers';
import { useLocation } from 'react-router';
const MyCreateForm: React.FC = () => {

    const [name, setName] = useState('');
    const [price, setPrice] = useState(0);
    const [description, setDesc] = useState('');
    const [showToast, setToast] = useState(false);


    const location = useLocation();
    const createBook = async () => {
        var data = {
            name, price, description
        }
        console.log(data);

        var response = await axios.post("http://192.168.169.177:63918/api/books", JSON.stringify(data), {
            headers: { 'Content-Type': 'application/json-patch+json' }
        });
        
        if (response.status == 200) {
            setToast(true);
            setInterval(() =>{
                location.pathname = "page/books";
                window.location.href = "page/books";
            }, 500);
        }
    }

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Create new Book</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent class="my-card" style={{ width: "100%" }}>
                <IonCard>
                    <IonCardHeader><IonText color="success">Create Form</IonText></IonCardHeader>
                    <IonCardContent>
                        <IonList>
                            <IonItem>
                                <IonLabel position="floating">Name</IonLabel>
                                <IonInput id="name" onIonChange={(e) => setName((e.target as HTMLInputElement).value)} ></IonInput>
                            </IonItem>
                            <IonItem>
                                <IonLabel position="floating">Description</IonLabel>
                                <IonTextarea onIonChange={(e) => setDesc((e.target as HTMLInputElement).value)}></IonTextarea>
                            </IonItem>
                            <IonItem>
                                <IonLabel position="floating">Price</IonLabel>
                                <IonInput type="number" placeholder="Enter Number" onIonChange={(e) => setPrice(parseInt((e.target as HTMLInputElement).value))}></IonInput>
                            </IonItem>
                        </IonList>
                        <IonButton color="success" onClick={() => createBook()} >
                            Create
                         </IonButton>
                    </IonCardContent>
                </IonCard>
            </IonContent>
            <IonToast
                color = "success"
                isOpen={showToast}
                onDidDismiss={() => setToast(false)}
                message="Create Success !"
                duration={500}
            />
        </IonPage>
    );
}


export default MyCreateForm;