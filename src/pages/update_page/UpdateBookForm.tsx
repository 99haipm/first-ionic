

import React, { useState } from 'react';
import { IonPage, IonHeader, IonToolbar, IonButtons, IonMenuButton, IonTitle, IonContent, IonCard, IonCardHeader, IonText, IonCardContent, IonList, IonItem, IonLabel, IonInput, IonTextarea, IonButton, IonToast } from '@ionic/react';
import { useParams } from 'react-router';
import axios from 'axios';

const UpdateForm: React.FC = () => {

    const {id} = useParams<{id : string}>();


    const [name,setName] = useState('');
    const [desc,setDesc] = useState('');
    const [price,setPrice] = useState(0);
    const [toast,setToast] = useState(false);
    const [status,SetStatus]=  useState(false);

    const fetchData = async () => {
      var response =  await axios.get(`http://192.168.169.177:63918/api/books/get-by-id?id=${id.substring(1,id.length)}`);
      if(response.status == 200){
          setName(response.data["name"]);
          setDesc(response.data["description"]);
          setPrice(parseInt(response.data["price"]));
          SetStatus(true);
      }
    }

    const updateData = async() =>{
        var data = {
            name : name,
            price : price,
            description : desc
        };
        console.log(data);

        var response = await axios.put(`http://192.168.169.177:63918/api/books?id=${id.substring(1,id.length)}`, JSON.stringify(data), {
            headers: { 'Content-Type': 'application/json-patch+json' }
        });
        
        if (response.status == 200) {
            setToast(true);
            setInterval(() =>{
                window.location.href = "page/books";
            }, 500);
        }
    }

    if(!status){
        fetchData();
    }
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Update Book</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent class="my-card" style={{ width: "100%" }}>
                <IonCard>
                    <IonCardHeader><IonText color="warning">Update Form</IonText></IonCardHeader>
                    <IonCardContent>
                        <IonList>
                            <IonItem>
                                <IonLabel position="floating">Name</IonLabel>
                                <IonInput value={name} onIonChange={(e) => setName((e.target as HTMLInputElement).value)}></IonInput>
                            </IonItem>
                            <IonItem>
                                <IonLabel position="floating">Description</IonLabel>
                                <IonTextarea value={desc} onIonChange={(e) => setDesc((e.target as HTMLInputElement).value)}></IonTextarea>
                            </IonItem>
                            <IonItem>
                                <IonLabel position="floating">Price</IonLabel>
                                <IonInput type="number" value={price} onIonChange={(e) => setPrice(parseInt((e.target as HTMLInputElement).value))} ></IonInput>
                            </IonItem>
                        </IonList>
                        <IonButton color="warning" onClick={()=>updateData()}>
                            update
                </IonButton>
                    </IonCardContent>
                </IonCard>
            </IonContent>
            <IonToast
                color = "success"
                isOpen={toast}
                onDidDismiss={() => setToast(false)}
                message="Update Success !"
                duration={500}
            />
        </IonPage>
    );
}


export default UpdateForm;